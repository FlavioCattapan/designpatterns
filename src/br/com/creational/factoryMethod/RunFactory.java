package br.com.creational.factoryMethod;

public class RunFactory {
	
	
	public static void main(String[] args) {
		
		// Factory Method permite as classes delegar para subclasses decidirem.
		// O factory method permite delegar a instancia��o para as subclasses.

		// Creator � declara o factory method (m�todo de fabrica��o) que retorna
		// o objeto da classe Product (produto). Este elemento tamb�m pode
		// definir uma implementa��o b�sica que retorna um objeto de uma classe
		// ConcreteProduct (produto concreto) b�sica;
		// ConcreteCreator � sobrescreve o factory method e retorna um objeto da
		// classe ConcreteProduct;
		// Product � define uma interface para os objectos criados pelo factory
		// method;
		// ConcreteProduct � uma implementa��o para a interface Product.
		
		FloorPlan floorPlan = new Apartment().createApart();
		
	}

}
