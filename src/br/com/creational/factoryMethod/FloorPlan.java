package br.com.creational.factoryMethod;

// Produto
import java.util.ArrayList;
import java.util.List;

public class FloorPlan {
	private List<ElementOfApart> rooms = new ArrayList<ElementOfApart>();

	public void addElement(ElementOfApart object) {
		rooms.add(object);
	}
}
