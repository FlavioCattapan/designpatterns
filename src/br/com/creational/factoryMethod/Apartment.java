package br.com.creational.factoryMethod;

//CREATOR
public class Apartment {
	public Room createRoom() {
		return new Room();
	}

	public Kitchen createKitchen() {
		return new Kitchen();
	}

	public FloorPlan createFloorPlan() {
		return new FloorPlan();
	}

	public FloorPlan createApart() {
		FloorPlan floorPlans = createFloorPlan();
		Room room1 = createRoom();
		floorPlans.addElement(room1);
		Room room2 = createRoom();
		floorPlans.addElement(room2);
		Kitchen kitchen = createKitchen();
		floorPlans.addElement(kitchen);
		return floorPlans;
	}
}
