package br.com.creational.factoryMethod;

public class ApartmentEconomic extends Apartment {
	@Override
	// Metodo sobrescrito cria uma cozinha americana
	public Kitchen createKitchen() {
		return new AmericanKitchen();
	}
}
