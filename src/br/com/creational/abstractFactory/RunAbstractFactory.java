package br.com.creational.abstractFactory;

public class RunAbstractFactory {
	public static void main(String[] args) {
		
		// Abstract Factory � um padr�o de projeto de software
		// (tamb�m conhecido como design pattern em ingl�s).
		// Este padr�o permite a cria��o de fam�lias de objetos relacionados ou
		// dependentes
		// por meio de uma �nica interface e sem que a classe concreta seja
		// especificada.
		
		// F�brica de objetos.
		// Fabrica os objetos de forma centralizada, se houver a necessidade de alterar a forma que as
		// implementa��es s�o fabricadas alteramos em somente um lugar.
		
		// N�o especificamos suas classes concretas.
		// A f�brica decide qual ser� a forma criada.
		Forma forma = Forma.getForma(new int[] { 5, 5 });
		forma.calcularArea();
		// N�o especificamos suas classes concretas.
		Forma forma2 = Forma.getForma(new int[] { 5 });
		forma2.calcularArea();
		
		
		// O padr�o Abstract Factory pode ser utilizado na implementa��o de um
		// toolkit que disponibilize
		// controles que funcionem em diferentes interfaces gr�ficas, tal como
		// Motif, GTK+ (GNOME) ou Qt (KDE).
		// Estas GUIs possuem diferentes padr�es de controles visuais e, para
		// facilitar a constru��o de
		// aplicativos que interajam facilmente com diferentes interfaces
		// gr�ficas, � interessante que se
		// defina interfaces comuns para acesso aos controles, independentemente
		// da GUI utilizada.
		// Este problema pode ser resolvido por meio de uma classe abstrata que
		// declara uma interface
		// gen�rica para cria��o dos controles visuais e de uma classe abstrata
		// para cria��o de cada tipo
		// de controle. O comportamento espec�fico, de cada um dos padr�es
		// tecnol�gicos contemplados,
		// � implementado por meio de uma classe concreta. O aplicativo, ou
		// "cliente", interage com o
		// toolkit por meio das classes abstratas sem ter conhecimento da
		// implementa��o das classes concretas.
		//
		// Um exemplo bem simplista seria um projeto com interface para Mobile e
		// para Desktop,
		// uma boa op��o para reaproveitar os mesmos controles de interface
		// seria criar pacotes
		// com classes abstratas e os pacotes com as classes concretas
		// implementando apenas as diferen�as.
		// Esse padr�o tamb�m se aplica na padroniza��o de ambientes, por
		// exemplo, tamanhos de bot�es,
		// fontes, cores de fundo, largura de bordas. Com isso e havendo uma
		// pol�tica que exija que os
		// desenvolvedores usem essas classes em vez das nativas da linguagem,
		// ajudar� a padronizar a
		// apar�ncia e comportamento das aplica��es.
		
	}
}
