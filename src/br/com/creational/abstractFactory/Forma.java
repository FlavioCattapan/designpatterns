package br.com.creational.abstractFactory;

public class Forma {
	
	public static Forma getForma(int[] attr) {
		if (attr.length == 1) {
			return new Circulo(attr[0]);
		}
		if (attr.length == 2) {
			return new Triangulo(attr[0], attr[1]);
		}
		return null;
	}

	public void calcularArea() {
	}
}
