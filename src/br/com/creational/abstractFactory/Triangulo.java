package br.com.creational.abstractFactory;

public class Triangulo extends Forma {
	int b;
	int h;

	public Triangulo(int b, int h) {
		this.b = b;
		this.h = h;
	}

	@Override
	public String toString() {
		return "Triangulo";
	}

	public void calcularArea() {
		// (b * h) / 2
		System.out.println((this.b * this.h) / 2);
	}
}
