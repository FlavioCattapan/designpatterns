package br.com.creational.abstractFactory;

public class Circulo extends Forma {
	int raio;

	public Circulo(int raio) {
		this.raio = raio;
	}

	@Override
	public String toString() {
		return super.toString();
	}

	public void calcularArea() {
		// PI * r^2
		System.out.println(Math.PI * Math.pow(this.raio, 2));
	}
}
