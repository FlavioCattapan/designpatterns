package br.com.creational.buider;

public class TriangleBuilder extends AShapeBuilder {
	public TriangleBuilder() {
		shape = new Shape("Triangle");
	}

	@Override
	public void buildSides() {
		shape.setParts("SIDES", "3");
	}

	@Override
	public void buildColor() {
		shape.setParts("COLOR", "Blue");
	}

	@Override
	public void buidArea() {
		shape.setParts("AREA", "8");
	}
}
