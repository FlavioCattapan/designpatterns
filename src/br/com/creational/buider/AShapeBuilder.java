package br.com.creational.buider;

public abstract class AShapeBuilder {
	// Visivel para os motores de contru��o do shape
	protected Shape shape;

	public Shape getShape() {
		return this.shape;
	}

	abstract public void buildSides();

	abstract public void buildColor();

	abstract public void buidArea();
}
