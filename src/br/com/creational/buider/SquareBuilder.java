package br.com.creational.buider;

public class SquareBuilder extends AShapeBuilder {
	public SquareBuilder() {
		shape = new Shape("Square");
	}

	@Override
	public void buildSides() {
		shape.setParts("SIDES", "4");
	}

	@Override
	public void buildColor() {
		shape.setParts("COLOR", "Green");
	}

	@Override
	public void buidArea() {
		shape.setParts("AREA", "10");
	}
}
