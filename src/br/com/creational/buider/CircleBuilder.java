package br.com.creational.buider;

public class CircleBuilder extends AShapeBuilder {
	public CircleBuilder() {
		shape = new Shape("Circle");
	}

	@Override
	public void buildSides() {
		shape.setParts("SIDES", "0");
	}

	@Override
	public void buildColor() {
		shape.setParts("COLOR", "Red");
	}

	@Override
	public void buidArea() {
		shape.setParts("AREA", "23,1");
	}
}
