package br.com.creational.buider.notaFiscal;

public class RunBuilder {

	public static void main(String[] args) {

		// Esconder o processo de cria��o de objetos complexos
		// Method chain
		
		NotaFiscalBuilder notalFiscalBuilder = new NotaFiscalBuilder();
		notalFiscalBuilder.paraEmpresa("RichNet")
		.comCnpj("11.125.685/0001-12")
		.comItem(new ItemDaNota("Item 1", 200.00))
		.comItem(new ItemDaNota("Item 2", 300.00))
		.comObservacoes("obs")
		.naDataAtual();
		
		NotaFiscal notaFiscal = notalFiscalBuilder.constroi();
		
		System.out.println(notaFiscal.getValorBruto());
		
		
		

	}

}
