package br.com.creational.buider;

public class Factory {
	public void contruct(AShapeBuilder shapeBuilder) {
		shapeBuilder.buidArea();
		shapeBuilder.buildColor();
		shapeBuilder.buildSides();
	}
}
