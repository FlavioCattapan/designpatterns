package br.com.creational.buider;

import java.util.HashMap;
import java.util.Map;
// Classe produto
// A classe n�o se preocupra como ela � contruida

public class Shape {
	private String type;
	private Map parts = new HashMap();

	public Shape(String type) {
		this.type = type;
	}

	public Object getParts(String key) {
		return this.parts.get(key);
	}

	public void setParts(String key, String value) {
		this.parts.put(key, value);
	}

	public String toString() {
		return "Shape :" + this.type + "  (Sides= " + this.getParts("SIDES")
				+ " Color=" + this.getParts("COLOR") + " Area="
				+ this.getParts("AREA") + ")";
	}
}
