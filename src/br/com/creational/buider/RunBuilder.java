package br.com.creational.buider;

public class RunBuilder {
	public static void main(String[] args) {

		// Constr�i um objeto complexo e suas partes sem anexar isso a
		// representa��o desse objeto.
		// Builder a m�quina que constr�i o objeto.
		// Existem formas diferentes de construir o objeto.
		// No produto n�o existe nada sobre a forma que o projeto � constru�do.

		// Constr�i quadrado
		AShapeBuilder builder1 = new SquareBuilder();
		// Constr�i C�rculo
		AShapeBuilder builder2 = new CircleBuilder();
		// Constr�i triangulo
		AShapeBuilder builder3 = new TriangleBuilder();

		// F�brica
		Factory factory = new Factory();
		factory.contruct(builder1);
		System.out.println(builder1.getShape());
		factory.contruct(builder2);
		System.out.println(builder2.getShape());
		factory.contruct(builder3);
		System.out.println(builder3.getShape());

		// Builder � um padr�o de projeto de software que permite a separa��o da
		// constru��o
		// de um objeto complexo da sua representa��o, de forma que o mesmo
		// processo de constru��o
		// possa criar diferentes representa��es.

		// Compara��o com o Abstract Factory

		// O padr�o Builder � muitas vezes comparado com o padr�o Abstract
		// Factory pois ambos
		// podem ser utilizados para a constru��o de objetos complexos.
		// A principal diferen�a entre eles � que o Builder constr�i objetos
		// complexos passo a passo e o Abstract Factory constr�i fam�lias de
		// objetos,
		// simples ou complexos, de uma s� vez.

	}
}
