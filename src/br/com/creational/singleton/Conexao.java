package br.com.creational.singleton;

public class Conexao {
	
	private static Conexao instaciaSingleton = null;
	
	// Construtor evita que a classe seja instanciada. Obteremos a referencia somente pela chamada do m�todo.
	public Conexao() {
		
	}

	// M�todo respons�vel por criar uma �nica instancia independente de quantas vezes for invocado.
	public static Conexao getInstancia() {
		if (instaciaSingleton == null) {
			instaciaSingleton = new Conexao();
		}
		return instaciaSingleton;
	}
}
