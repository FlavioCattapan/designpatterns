package br.com.creational.singleton;

public class SingletonLog {
    // Construtor privado. Suprime o construtor p�blico padrao.
     private SingletonLog() {
     // Leitura da configura��o de log. Normalmente descrita em um arquivo.
     }

     // Faz o log de eventos da aplicacao
     public void doLog(String eventDescription) {

     }

     //Retorna a instancia �nica da classe SingletonLog
     public static SingletonLog getInstance() {
         return SingletonLogHolder.instance;
     }

     //Classe auxiliar para criacao da instancia. Evita problemas de sincronizacao de threads.
     private static class SingletonLogHolder {
            private static SingletonLog instance = new SingletonLog();
     }
 }
