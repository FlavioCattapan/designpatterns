package br.com.creational.singleton;

public class RunSingleton {

	public static void main(String[] args) {

		// Singleton � um padr�o de projeto de software (do ingl�s Design
		// Pattern).
		// Este padr�o garante a exist�ncia de apenas uma inst�ncia de uma
		// classe,
		// mantendo um ponto global de acesso ao seu objeto.

		// Nota lingu�stica: O termo vem do significado em ingl�s quando se
		// resta apenas uma carta nas m�os,
		// num jogo de baralho.

		// Alguns projetos necessitam que algumas classes tenham apenas uma
		// inst�ncia. Por exemplo, em uma aplica��o que precisa de uma
		// infraestrutura de log de dados, pode-se implementar uma classe no
		// padr�o singleton. Desta forma existe apenas um objeto respons�vel
		// pelo log em toda a aplica��o que � acess�vel unicamente atrav�s da
		// classe singleton.

		Conexao conexao = Conexao.getInstancia();

		// O objeto ser� criado uma �nica vez, independente de quantas vezes for
		// invocado.

		Conexao conexao2 = Conexao.getInstancia();

		// Onde Usar

		// Quando voc� necessita de somente uma inst�ncia da classe, por
		// exemplo,
		// a conex�o com banco de dados, vamos supor que voc� ter� que chamar
		// diversas
		// vezes a conex�o com o banco de dados em um c�digo na mesma execu��o,
		// se voc� instanciar toda vez a classe de banco, haver� grande perda de
		// desempenho,
		// assim usando o padr�o singleton, � garantida que nesta execu��o ser�
		// inst�nciada a classe
		// somente uma vez. Lembrando que este pattern � considerado por muitos
		// desenvolvedores
		// um antipattern, ent�o, cuidado onde for utiliz�-lo. [1]
		//

	}
}
