package br.com.creational.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {


	public Connection getConnection(){
		
		Connection connection = null;
		try {
			connection = DriverManager.
					getConnection("jdbc:postgresql://host:port/database", "root", "12345");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		
		return connection;
		
	}
	
	
}
