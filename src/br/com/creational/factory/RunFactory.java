package br.com.creational.factory;

import java.sql.Connection;

public class RunFactory {

	public static void main(String[] args) {

	//	Factory, um padr�o que fornece a possibilidade de criarmos 
	//	uma fabrica para cria��o dos nossos objetos em tempo de execu��o,
	//	deixando o cliente isento de instanciar a classe ganhando um dinamismo para a aplica��o

		Connection connection = new ConnectionFactory().getConnection();
		
		
	}

}
