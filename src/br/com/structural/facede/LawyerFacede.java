package br.com.structural.facede;

public class LawyerFacede {
	public static final int CRIMINAL = 0;
	public static final int CIVIL = 1;

	public Processo getProcess(int type) {
		switch (type) {
		case CRIMINAL:
			Justice criminalJustice = new CriminalJustice();
			return criminalJustice.getProcess();
		case CIVIL:
			Justice civilJustice = new CivilJustice();
			return civilJustice.getProcess();
		default:
			return null;
		}
	}
}
