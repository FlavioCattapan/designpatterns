package br.com.structural.facede;

public class RunFacade {
	public static void main(String[] args) {
		
		
		
		// Em padr�es de projeto de software, um fa�ade
		// ("fachada" em ingl�s, palavra de origem francesa; comumente escrito
		// em ingl�s sem o cedilha,
		// mas ainda com som de /s/: facade) � um objeto que disponibiliza uma
		// interface simplificada
		// para uma das funcionalidades de uma API, por exemplo. Um fa�ade pode:
		//
		// - tornar uma biblioteca de software mais f�cil de entender e usar;
		// - tornar o c�digo que utiliza esta biblioteca mais f�cil de entender;
		// - reduzir as depend�ncias em rela��o �s caracter�sticas internas de
		// uma biblioteca, trazendo flexibilidade no desenvolvimento do sistema;
		// - envolver uma interface mal desenhada, com uma interface melhor
		// definida.
		//
		// Um fa�ade � um padr�o de projeto (design pattern) do tipo estrutural.
		// Os fa�ades s�o muito comuns em projeto orientados a objeto.
		// Por exemplo, a biblioteca padr�o da linguagem Java cont�m d�zias de
		// classes para processamento do arquivo fonte de um caractere, gera��o
		// do seu desenho
		// geom�trico e dos pixels que formam este caractere.
		// Entretanto, a maioria dos programadores Java n�o se preocupam com
		// esses
		// detalhes, pois a biblioteca cont�m as classes do tipo fa�ade (Font e
		// Graphics)
		// que oferecem m�todos simples para as opera��es relacionadas com
		// fontes
		// N�o colocar regras de neg�cios
		// O nao precisar conhecer todas as classes
		LawyerFacede lawyerFacede = new LawyerFacede();
		Processo process = lawyerFacede.getProcess(LawyerFacede.CIVIL);
	}
}
