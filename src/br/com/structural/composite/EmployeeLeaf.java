package br.com.structural.composite;

// Folha ponta da �rvore n�o pode ter filhos.
public class EmployeeLeaf extends ElementsCompany {
	public EmployeeLeaf(String name) {
		super(name);
	}

	@Override
	public void add(ElementsCompanyIF element) {
		System.out.println("Cannot add here");
	}

	@Override
	public void remove(ElementsCompanyIF element) {
		System.out.println("Cannot remove");
	}

	@Override
	public void diplay(String ident) {
		System.out.println(ident + this);
	}

	public static void main(String[] args) {
	}

	@Override
	public String toString() {
		return getName();
	}
}
