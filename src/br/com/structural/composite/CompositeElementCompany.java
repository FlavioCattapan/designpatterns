package br.com.structural.composite;

import java.util.ArrayList;
import java.util.List;

// Composite
public class CompositeElementCompany extends ElementsCompany {
	
	private List<ElementsCompanyIF> elementsCompanyIFs = new ArrayList<ElementsCompanyIF>();

	public CompositeElementCompany(String name) {
		super(name);
	}

	@Override
	public void add(ElementsCompanyIF element) {
		elementsCompanyIFs.add(element);
	}

	@Override
	public void remove(ElementsCompanyIF element) {
		elementsCompanyIFs.remove(element);
	}

	// Imprime ele e sua estrutura hierárquica
	@Override
	public void diplay(String ident) {
		System.out.println(ident + this);
		ident += "-";
		for (ElementsCompanyIF element : elementsCompanyIFs) {
			// chama o display dos filhos cada metodo chama novamente o display
			// do filho ....
			element.diplay(ident);
		}
	}

	@Override
	public String toString() {
		return getName();
	}
}
