package br.com.structural.composite;

public class Client {
	public static void main(String[] args) {

		// Compor objetos em estruturas de �rvore para representarem hierarquias
		// parte-todo.
		// Composite permite aos clientes tratarem de maneira uniforme objetos
		// individuais e
		// composi��es de objetos.

		// O Componente (ElementoDoQuestionario):
		// � a interface que define m�todos comuns �s classes dentro da
		// composi��o e,
		// opcionalmente, define uma interface para acessar o objeto �pai� de um
		// componente.

		// A Folha (Questao) � um componente que, como o nome indica,
		// n�o possui filhos (est� nas extremidades da �rvore).

		// O Composto (Bloco) � um componente que, como o nome indica,
		// � composto de outros componentes, que podem ser folhas ou outros
		// compostos.

		CompositeElementCompany empresa = new CompositeElementCompany(
				"Empresa X");
		CompositeElementCompany departA = new CompositeElementCompany(
				"Departamento A");
		CompositeElementCompany departB = new CompositeElementCompany(
				"Departamento B");
		// adiciona departamentos
		empresa.add(departA);
		empresa.add(departB);
		// adiciona empregados no departamento A - elementos folha
		departA.add(new EmployeeLeaf("Aline"));
		departA.add(new EmployeeLeaf("Marcos"));
		// adiciona empregados no departamento B - elementos folha
		departB.add(new EmployeeLeaf("Fabio"));
		departB.add(new EmployeeLeaf("Isabela"));
		empresa.diplay("-");
	}
}
