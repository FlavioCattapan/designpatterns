package br.com.structural.flyweight;

import java.util.List;

public class Piano {

	public void toca(List<Nota> musica){
		
		for(Nota nota : musica){
			System.out.println(nota.simbolo());
		}
		
		
	}
	
	
}
