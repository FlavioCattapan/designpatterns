package br.com.structural.flyweight;

import java.util.Arrays;
import java.util.List;

public class RunFlyweight {

	public static void main(String[] args) {
		
//		Flyweight � um padr�o de projeto de software apropriado quando
//		v�rios objetos devem ser manipulados em mem�ria sendo que muitos
//		deles possuem informa��es repetidas. Dado que o recurso de mem�ria 
//		� limitado, � poss�vel segregar a informa��o repetida em um objeto 
//		adicional que atenda as caracter�sticas de imutabilidade e comparabilidade 
//		(que consiga ser comparado com outro objeto afim de determinar se ambos carregam
//		a mesma informa��o).

//		Um exemplo � o processador de texto.
//		Cada caractere representa um objeto que possui uma fam�lia de fonte, 
//		um tamanho de fonte e outras informa��es sobre o s�mbolo. Como imaginado,
//		um documento grande com tal estrutura de dados facilmente ocuparia toda a 
//		mem�ria dispon�vel no sistema. Para resolver o problema, como muitas dessas
//		informa��es s�o repetidas, o flyweight � usado para reduzir os dados. 
//		Cada objeto de caractere cont�m uma refer�ncia para outro objeto com suas
//		respectivas propriedades.
		
		
//		O Flyweight usa compartilhamento para suportar grandes quantidades de objetos de forma eficiente.
//		A rede telef�nica comutada p�blica � um exemplo de um Flyweight.
//		Existem v�rios recursos, tais como geradores de tom de discagem, geradores de toque,
//		e receptores de d�gitos que devem ser compartilhados entre todos os assinantes.
//		Um assinante n�o tem conhecimento de quantos recursos est�o na piscina quando ele
//		ou ela levanta a m�o definido para fazer uma chamada.
//		Tudo o que importa para os assinantes � que o tom de discagem � fornecido,
//		d�gitos s�o recebidos, ea chamada � conclu�da.
		
		
		
		NotasMusicais notasMusicais = new NotasMusicais();
		
		List<Nota> musica = Arrays.asList(
				notasMusicais.pega("do"),
				notasMusicais.pega("re"),
				notasMusicais.pega("mi"),
				notasMusicais.pega("fa"),
				notasMusicais.pega("fa"),
				notasMusicais.pega("do")
				) ;
		
		Piano piano = new Piano();
		
		piano.toca(musica);
		
		
		
		
		

	}

}
