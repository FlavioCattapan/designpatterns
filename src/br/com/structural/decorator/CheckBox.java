package br.com.structural.decorator;

public class CheckBox extends Component {
	public CheckBox() {
		name = "Checkbox";
	}

	String getHtml() {
		return "<input type='checkbox' value='Clique Aqui' name='ch' />";
	}
}
