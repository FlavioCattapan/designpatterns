package br.com.structural.decorator.imposto;


public abstract class Imposto {

	protected Imposto outroImposto;
	
	public Imposto(Imposto outroImposto) {
		super();
		this.outroImposto = outroImposto;
	}
	
	public Imposto() {

	}

	protected double calculaDoOutroImposto(Orcamento orcamento){
		if(outroImposto == null){
			return 0;
		}
		return outroImposto.calcula(orcamento);
	}

	abstract double calcula(Orcamento orcamento);
	
}
