package br.com.structural.decorator.imposto;


/**
 * @author Flavio
 *
 */
public class RunImpostoComplexos {
	
	public static void main(String[] args) {
		
		Imposto iss = new ISS(new ICMS());
		
		Orcamento orcamento = new Orcamento(500.00);
		
		double valor = iss.calcula(orcamento);
		
		System.out.println(valor);
		
	}

}
