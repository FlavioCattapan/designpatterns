package br.com.structural.decorator.imposto;


public class Orcamento {
	
	private final Double valor;
	
	public Orcamento(Double valor) {
		super();
		this.valor = valor;
	}

	public Double getValor() {
		return valor;
	}
	

}
