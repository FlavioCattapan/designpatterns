package br.com.structural.decorator.imposto;

public class ISS extends Imposto {
	
	public ISS() {
		super();
	}

	public ISS(Imposto outroImposto) {
		super(outroImposto);
	}

	@Override
	public double calcula(Orcamento orcamento) {
		return orcamento.getValor() * 0.06 + calculaDoOutroImposto(orcamento);
	}
	
	
}
