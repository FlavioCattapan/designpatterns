package br.com.structural.decorator;

public class FieldSet extends ContainerDecorator {
	Component component;

	public FieldSet(Component component) {
		this.component = component;
	}

	@Override
	public String getName() {
		return getName() + " - " + "FIELDSET";
	}

	@Override
	String getHtml() {
		String temp = "<fieldset>";
		temp += "<legend> Titulo  </legend>";
		temp += component.getHtml();
		return temp + "</fieldset>";
	}
}
