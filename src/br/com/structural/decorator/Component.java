package br.com.structural.decorator;

public abstract class Component {
	protected String name;

	abstract String getHtml();

	public String getName() {
		return name;
	}
}
