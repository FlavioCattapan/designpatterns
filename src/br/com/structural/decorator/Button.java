package br.com.structural.decorator;

public class Button extends Component {
	public Button() {
		name = "Button";
	}

	@Override
	String getHtml() {
		return "<input type='button' value='Clique Aqui' />";
	}
}
