package br.com.structural.decorator;

public class RunDecorator {
	public static void main(String[] args) {
		Component checkBox = new CheckBox();
		ContainerDecorator div = new FieldSet(new Div(checkBox));
		System.out.println(div.getHtml());
		
//      Junta comportamento que est�o em classes diferentes
		
		
//		Decorator ou wrapper, � um padr�o de projeto de software que permite 
//		adicionar um comportamento a um objeto j� existente em tempo de execu��o,
//		ou seja, agrega dinamicamente responsabilidades adicionais a um objeto.

//		Inten��o
//		Acrescentar responsabilidades a um objeto dinamicamente
//		Prover alternativa flex�vel ao uso de subclasses para se estender a funcionalidade de uma classe

//		Motiva��o
//	    Objeto usado possui as funcionalidades b�sicas, mas � necess�rio adicionar funcionalidades adicionais a ele que podem ocorrer antes ou depois da funcionalidade b�sica
//	    Funcionalidades devem ser adicionadas em instancias individuais e n�o na classe.

//		Consequ�ncias
//	    Mais flexibilidade do que heran�a
//	    Adi��o ou remo��o de responsabilidades em tempo de execu��o
//	    Adi��o da mesma propriedade mais de uma vez
//	    Evita o excesso de funcionalidades nas classes
//		Decorator e seu componente n�o s�o id�nticos
//		Compara��es tornam-se mais complexas
//		Resulta em um design que tem v�rios pequenos objetos, todos parecidos

//		Aplicabilidade
//		Acrescentar ou remover responsabilidades a objetos individuais dinamicamente,
//		de forma transparente
//		Evitar a explos�o de subclasses para prover todas as combina��es de responsabilidades

//		Padr�es relacionados
//	    Adapter: Decorator muda comportamento; Adapter muda interface
//	    Composite: Pode ser visto como um composite com um �nico componente; 
//	    por�m, n�o tem inten��o de agregar objetos
//	    Strategy: Decorator envolve o objeto; Strategy muda o funcionamento interno

		
	}
}
