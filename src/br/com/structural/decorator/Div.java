package br.com.structural.decorator;

public class Div extends ContainerDecorator {
	Component component;

	public Div(Component component) {
		this.component = component;
	}

	@Override
	public String getName() {
		return getName() + " - " + "DIV";
	}

	@Override
	String getHtml() {
		String temp = "<div>";
		temp += component.getHtml();
		return temp + "</div>";
	}
}
