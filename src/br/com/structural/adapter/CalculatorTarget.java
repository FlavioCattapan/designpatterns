package br.com.structural.adapter;

public interface CalculatorTarget {
	double complexCalculation(double x);
}
