package br.com.structural.adapter;

public class Client {
	public static void main(String[] args) {
		
		// �til para reinscrever sistemas adapta uma interface feia antiga a uma
		// interface nova do sistema.
		// Classe que n�o conseguimos mexer agora.

		// Converter uma interface e outra esperada pelo cliente.
		// Permite a comunica��o entre classes que n�o poderiam trabalhar juntas
		// devido a incompatibilidade das interfaces.
		// Classe a ser adaptada - adaptee
		// Target - recebe a classe adaptada.
		// Adapter recebe a classe adaptada e pluga no alvo, ou seja a classe adaptee no target.
		CalculatorTarget calculator = new Adapter();
		System.out.println(calculator.complexCalculation(10));
		
		
		// Adapter, tamb�m conhecido como Wrapper, � um padr�o de projeto de
		// software
		// (do ingl�s design pattern). Este padr�o � utilizado para 'adaptar' a
		// interface de uma classe.
		// O Adapter permite que classes com interfaces incompat�veis possam
		// interagir.

		// Adapter permite que um objeto cliente utilize servi�os de outros
		// objetos com interfaces
		// diferentes por meio de uma interface �nica.
	}
}
