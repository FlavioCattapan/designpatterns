package br.com.structural.adapter;

public class Adapter extends MathLibAdaptee implements CalculatorTarget {
	@Override
	public double complexCalculation(double x) {
		double y = squareRoot(x) - 1;
		return sum(y, x * 2);
	}
}
