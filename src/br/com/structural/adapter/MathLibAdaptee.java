package br.com.structural.adapter;

public class MathLibAdaptee {
	public double squareRoot(double x) {
		return Math.sqrt(x);
	}

	public double sum(double a, double b) {
		return a + b;
	}
}
