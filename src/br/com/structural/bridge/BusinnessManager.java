package br.com.structural.bridge;

// Abstra��o
public class BusinnessManager {
	
	protected DataAccess dataAccess;

	public void setDataAccess(DataAccess dataAccess) {
		this.dataAccess = dataAccess;
	}

	public void insert(String name) {
		dataAccess.create(name);
	}

	public void delete(String name) {
		dataAccess.remove(name);
	}

	public void print() {
		dataAccess.print();
	}
}
