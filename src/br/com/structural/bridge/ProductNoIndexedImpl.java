package br.com.structural.bridge;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ProductNoIndexedImpl extends DataAccess {
	private List productList = new ArrayList();

	@Override
	public void create(String name) {
		productList.add(name);
	}

	@Override
	public void remove(String name) {
		productList.remove(name);
	}

	@Override
	public void print() {
		Iterator i = productList.iterator();
		while (i.hasNext()) {
			System.out.println(i.next());
		}
	}
}
