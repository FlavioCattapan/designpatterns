package br.com.structural.bridge;

// Implementação
public abstract class DataAccess {
	abstract public void create(String name);

	abstract public void remove(String name);

	abstract public void print();
}
