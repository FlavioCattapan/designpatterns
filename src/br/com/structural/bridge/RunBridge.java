package br.com.structural.bridge;

// Classe client
public class RunBridge {
	public static void main(String[] args) {

		// Bridge � um padr�o de projeto de software, ou design pattern em
		// ingl�s,
		// utilizado quando � desej�vel que uma interface (abstra��o) possa
		// variar independentemente
		// das suas implementa��es.
		//
		// Imagine um sistema gr�fico de janelas que deve ser port�vel para
		// diversas plataformas.
		// Neste sistema s�o encontrados diversos tipos de janelas, como �cones,
		// di�logos, etc.
		// Estas janelas formam uma hierarquia que cont�m uma abstra��o das
		// janelas (classe base).
		// Normalmente, a portabilidade seria obtida criando-se especializa��es
		// dos tipos de janelas
		// para cada uma das plataformas suportadas. O problema com essa solu��o
		// reside na complexidade
		// da hierarquia gerada e na depend�ncia de plataforma que existir� nos
		// clientes do sistema.
		//
		// Atrav�s do padr�o Bridge, a hierarquia que define os tipos de janelas
		// � separada da
		// hierarquia que cont�m a implementa��o. Desta forma todas as opera��es
		// de Janela s�o
		// abstratas e suas implementa��es s�o escondidas dos clientes.

		// Principal objetivo separar a abstra��o da implementa��o.
		// A hierarquia de classes pode crescer.
		// Posso compilar as partes separadas.
		// BusinnessManager � a abstra��o
		BusinnessManager productsManager = new ProductBussinessObject();
		// N�o indexa n�o cria o ID
		DataAccess noIndexed = new ProductNoIndexedImpl();
		// Indexada cria o ID
		DataAccess indexed = new ProductIndexedImpl();
		// posso alterar aqui de noIndexed para indexed sem altera��es no
		// cliente
		// estou acessando uma abstra��o que tem uma ponte para a implementa��o
		// posso trocar a implementa��o que nao vai impactar no codico do
		// sistema
		productsManager.setDataAccess(noIndexed);
		productsManager.insert("Milk");
		productsManager.insert("Tea");
		productsManager.insert("Bean");
		productsManager.insert("Coffe");
		productsManager.insert("Pizza");
		productsManager.print();
		productsManager.delete("Pizza");
		productsManager.print();

	}
}
