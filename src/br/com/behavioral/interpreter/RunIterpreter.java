package br.com.behavioral.interpreter;

import java.util.ArrayList;

public class RunIterpreter {

	public static void main(String[] args) {
		
//		Interpreter � um padr�o de projeto de software que especifica como entender frases 
//		em uma determinada linguagem de programa��o.
//		O padr�o de projeto Interpreter pode ser utilizado para representar e
//		resolver problemas recorrentes que possam ser expressos sob a forma de uma 
//		linguagem formal simples.
//		Gram�ticas simples n�o precisam ser interpretadas por c�digos criados atrav�s
//		de geradores de analisadores sint�ticos. Para isso, podemos criar uma simples
//		hierarquia de classes baseada na gram�tica que, atrav�s de recurs�o, devolve a 
//		interpreta��o do c�digo de entrada. � justamente isso que apresenta o padr�o Interpreter:
//		uma solu��o elegante na interpreta��o de pequenas gram�ticas.
		
		
		    ArrayList<NumeroRomanoInterpreter> interpretadores = new ArrayList<NumeroRomanoInterpreter>();
		    interpretadores.add(new QuatroDigitosRomano());
		    interpretadores.add(new TresDigitosRomano());
		    interpretadores.add(new DoisDigitosRomano());
		    interpretadores.add(new UmDigitoRomano());
		 
		    String numeroRomano = "CXCIV";
		    Contexto contexto = new Contexto(numeroRomano);
		 
		    for (NumeroRomanoInterpreter numeroRomanoInterpreter : interpretadores) {
		        numeroRomanoInterpreter.interpretar(contexto);
		    }
		 
		    System.out.println(numeroRomano + " = "
		            + Integer.toString(contexto.getOutput()));
		
		
	}
	
}
