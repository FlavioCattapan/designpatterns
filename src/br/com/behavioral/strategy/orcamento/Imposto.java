package br.com.behavioral.strategy.orcamento;

public interface Imposto {
	
	double calcula(Orcamento orcamento);

}
