package br.com.behavioral.strategy.orcamento;

public class ICMS implements Imposto{

	@Override
	public double calcula(Orcamento orcamento) {
		return orcamento.getValor() * 0.1;    	
	}

}
