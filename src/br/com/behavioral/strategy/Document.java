package br.com.behavioral.strategy;

public abstract class Document {
	private DrawBehavior drawBehavior;

	// como a implementa��o de desenho n�o est� aqui ser� adicionado em
	// tempo de execu��o
	// baixo acoplamento entre documento e o comportamento de desenhar
	public void performDraw() {
		drawBehavior.draw();
	}

	public void setDrawBehavior(DrawBehavior drawBehavior) {
		this.drawBehavior = drawBehavior;
	}

	protected String text;

	public String getText() {
		return text;
	}

	public void addText(String text) {
		this.text += text;
	}
}
