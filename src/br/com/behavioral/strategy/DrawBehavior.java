package br.com.behavioral.strategy;

public interface DrawBehavior {
	void draw();
}
