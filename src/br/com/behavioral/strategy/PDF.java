package br.com.behavioral.strategy;

public class PDF implements DrawBehavior {
	@Override
	public void draw() {
		System.out.println("Print PDF");
	}
}
