package br.com.behavioral.strategy;

public class RunStrategy {
	public static void main(String[] args) {

		// Strategy � um padr�o de projeto de software (do ingl�s design
		// pattern).
		// O objetivo � representar uma opera��o a ser realizada sobre os
		// elementos de
		// uma estrutura de objetos. O padr�o Strategy permite definir novas
		// opera��es
		// sem alterar as classes dos elementos sobre os quais opera.
		// Definir uma fam�lia de algoritmos e encapsular cada algoritmo como
		// uma classe,
		// permitindo assim que elas possam ser trocados entre si. Este padr�o
		// permite que
		// o algoritmo possa variar independentemente dos clientes que o
		// utilizam.

		Document doc = new Legal();
		doc.setDrawBehavior(new PDF());
		doc.performDraw();
		doc.setDrawBehavior(new Html());
		doc.performDraw();

		// Aplica��o

		// Utilizar o padr�o Strategy quando:

		// �um objeto deve ser parametrizado com um de v�rios algoritmos, os
		// quais podem
		// ser encapsulados e representados por uma �nica interface.

	}
}
