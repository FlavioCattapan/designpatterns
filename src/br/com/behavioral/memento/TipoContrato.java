package br.com.behavioral.memento;

public enum TipoContrato {
	
	NOVO,
	EM_ANDAMENTO,
	ACERTADO,
	CONCLUIDO

}
