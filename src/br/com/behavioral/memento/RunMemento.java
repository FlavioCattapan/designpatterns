package br.com.behavioral.memento;

import java.util.Calendar;

public class RunMemento {
	
	
	public static void main(String[] args) {
		
//		Memento � um padr�o de projeto que permite armazenar o estado interno de um objeto
//		em um determinado momento, para que seja poss�vel retorn�-lo a este estado, caso necess�rio.

//		Tr�s objetos est�o envolvidos na implementa��o do padr�o Memento.
//		Originador � o objeto cujo estado se deseja capturar.
//		Memento � o objeto definido dentro da classe Originador,
//		com modificador de acesso privado, cujo estado do objeto originador ser� armazenado.
//		Cliente � o objeto que acessar� o originador, e deseja desfazer qualquer mudan�a efetuada,
//		caso necess�rio.

//		O cliente deve requisitar um objeto memento, 
//		antes de se valer do originador. Ap�s efetuar as opera��es desejadas no originador,
//		o cliente devolve a este o objeto memento, caso deseje desfazer qualquer altera��o.

//		O objeto memento n�o permite o acesso de qualquer classe al�m da classe originador.
//		Assim, tal padr�o mostra-se �til por n�o violar o conceito de encapsulamento.
		
//		As capturas Memento e externaliza o estado interno de um objeto,
//		de modo que o objeto pode ser restaurado para que o estado mais tarde.
//		Este padr�o � comum entre fazer-it-yourself repara��o mec�nica de freios a 
//		tambor em seus carros. Os tambores s�o removidos de ambos os lados, expondo
//		tanto os trav�es esquerdo e direito.
//		Apenas um lado est� desmontado, e do outro lado serve como uma lembran�a de
//		como as partes se encaixam freio [8]. Somente depois que o trabalho foi conclu�do
//		em um lado � o outro lado desmontado.
//		Quando o segundo lado � desmontado, o primeiro lado actua como a recorda��o.
		
		Historico historico = new Historico();
		Contrato contrato = new Contrato(Calendar.getInstance(), "Mauricio", TipoContrato.NOVO);
		historico.adiciona(contrato.salvaEstado());
		
		contrato.avanca();
		historico.adiciona(contrato.salvaEstado());
		
		contrato.avanca();
		historico.adiciona(contrato.salvaEstado());
		
		Estado estadoAnterior = historico.pega(2);
		
		System.out.println(estadoAnterior.getEstado().getTipo());
		
		
		
		
		
	}
	

}
