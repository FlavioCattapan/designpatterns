package br.com.behavioral.memento;

public class Estado {

	private Contrato contrato;

	public Estado(Contrato contrato) {
		super();
		this.contrato = contrato;
	}
	
	public Contrato getEstado(){
		return contrato;
	}
	
}
