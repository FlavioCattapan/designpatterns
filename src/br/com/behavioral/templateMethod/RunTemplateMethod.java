package br.com.behavioral.templateMethod;

public class RunTemplateMethod {
	public static void main(String[] args) {
		
		// Cria um m�todo gen�rico em uma classe abstrata e passa a responsabilidade da implementa��o para as subclasses.
		
		// Implementa o m�todo handleEfetuaOperacao
		Operacao soma = new Soma();
		
		soma.executaOperacacao(14, 4);
		
		// Implementa o m�todo handleEfetuaOperacao
		Operacao subtracao = new Subtracao();
		
		subtracao.executaOperacacao(14, 4);
	}
}
