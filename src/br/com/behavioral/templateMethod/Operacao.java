package br.com.behavioral.templateMethod;

abstract public class Operacao {
	
	//  As subclasses dever�o implementar esse m�todo.
	abstract int handleEfetuaOperacao(int valorA, int valorB);

	public void executaOperacacao(int valorA, int valorB) {
		// codigos
		int result = handleEfetuaOperacao(valorA, valorB);
		System.out.println("O resultado da opera��o � :" + result);
	}
}
