package br.com.behavioral.templateMethod.imposto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Orcamento {
	
	private final Double valor;
	
	private final List<Item> items;

	public Orcamento(Double valor) {
		super();
		this.valor = valor;
		this.items = new ArrayList<Item>();
	}

	public Double getValor() {
		return valor;
	}
	
	public void adicionaItem(Item item){
		items.add(item);
	}
	

	public List<Item> getItems() {
		return Collections.unmodifiableList(items);
	}
	
	
	
	

}
