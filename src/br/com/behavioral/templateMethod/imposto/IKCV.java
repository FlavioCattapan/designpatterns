package br.com.behavioral.templateMethod.imposto;


public class IKCV extends TemplateDeImpostoCondicional {

	private boolean temItemMaior100ReaisNo(Orcamento orcamento) {
		for(Item item : orcamento.getItems()){
			if(item.getValor() > 100) return true;
		}
		return false;
	
	}

	@Override
	double maximaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * 0.10;
	}

	@Override
	double minimaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * 0.06;
	}

	@Override
	boolean deveUsarMaximaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() > 500 && temItemMaior100ReaisNo(orcamento);
	}

	
	
	
	
}
