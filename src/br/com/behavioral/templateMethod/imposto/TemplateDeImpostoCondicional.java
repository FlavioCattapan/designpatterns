package br.com.behavioral.templateMethod.imposto;


public abstract class TemplateDeImpostoCondicional implements Imposto {
	
	
	public double calcula(Orcamento orcamento){
		
		if(deveUsarMaximaTaxacao(orcamento)){
			return maximaTaxacao(orcamento);
		}else{
			return minimaTaxacao(orcamento);
		}
		
	}

	abstract double maximaTaxacao(Orcamento orcamento);

	abstract double minimaTaxacao(Orcamento orcamento);

	abstract boolean deveUsarMaximaTaxacao(Orcamento orcamento);
	
	

}
