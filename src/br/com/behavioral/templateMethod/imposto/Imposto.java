package br.com.behavioral.templateMethod.imposto;

public interface Imposto {
	
	double calcula(Orcamento orcamento);

}
