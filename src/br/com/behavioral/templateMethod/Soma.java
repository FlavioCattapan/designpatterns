package br.com.behavioral.templateMethod;

public class Soma extends Operacao {
	@Override
	int handleEfetuaOperacao(int valorA, int valorB) {
		return valorA + valorB;
	}
}
