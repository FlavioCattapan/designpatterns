package br.com.behavioral.state.desconto;

public class Orcamento {
	
	private Double valor;
	
	private EstadoDeUmOrcamento estadoAtual;

	public Orcamento(Double valor) {
		super();
		estadoAtual = new EmAprovacao();
		this.valor = valor;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public void aplicaDescontoExtra(){
		estadoAtual.aplicaDescontoExtra(this);
	}

	public EstadoDeUmOrcamento getEstadoAtual() {
		return estadoAtual;
	}

	public void setEstadoAtual(EstadoDeUmOrcamento estadoAtual) {
		this.estadoAtual = estadoAtual;
	}
	
	public void aprova(){
		estadoAtual.aprova(this);
	}

	public void reprova(){
		estadoAtual.reprova(this);
	}
	
	public void finaliza() {
		estadoAtual.finaliza(this);
	}
	
}
