package br.com.behavioral.state.desconto;

public class Aprovado implements EstadoDeUmOrcamento{

	@Override
	public void aplicaDescontoExtra(Orcamento orcamento) {
		orcamento.setValor(orcamento.getValor() * 0.02);
	}

	@Override
	public void aprova(Orcamento orcamento) {
		new RuntimeException("Or�amento j� est� apovado");
	}

	@Override
	public void reprova(Orcamento orcamento) {
        new RuntimeException("Or�amentos aprovados n�o podem ser reprovados");
	}

	@Override
	public void finaliza(Orcamento orcamento) {
		orcamento.setEstadoAtual(new Finalizado());
	}

}
