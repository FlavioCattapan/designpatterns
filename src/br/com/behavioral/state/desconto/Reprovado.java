package br.com.behavioral.state.desconto;

public class Reprovado implements EstadoDeUmOrcamento {

	@Override
	public void aplicaDescontoExtra(Orcamento orcamento) {
        throw new RuntimeException("Or�amentos aprovados n�o recebem desconto extra!");		
	}

	@Override
	public void aprova(Orcamento orcamento) {
       throw new RuntimeException("Or�amentos reprovados n�o podem ser aprovados");		
	}

	@Override
	public void reprova(Orcamento orcamento) {
		throw new RuntimeException("Or�amentos j� est� reprovado");
	}

	@Override
	public void finaliza(Orcamento orcamento) {
         orcamento.setEstadoAtual(new Finalizado());    
	}

}
