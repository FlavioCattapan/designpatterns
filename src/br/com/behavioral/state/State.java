package br.com.behavioral.state;

public abstract class State {
	protected Order order;

	public State(Order order) {
		this.order = order;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public void insertService(Service service) {
		System.out.println("ERROR");
	}

	public void removeService(Service service) {
		System.out.println("ERROR");
	}

	public void cancelIOS() {
		System.out.println("ERROR");
	}

	public void requestPayment() {
		System.out.println("ERROR");
	}

	public void printInvoiced() {
		System.out.println("ERROR");
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
