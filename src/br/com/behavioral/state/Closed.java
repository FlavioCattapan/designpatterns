package br.com.behavioral.state;

public class Closed extends State {
	public Closed(Order order) {
		super(order);
	}

	@Override
	public void insertService(Service service) {
		order.getServices().add(service);
		order.setState(order.getOpen());
	}

	@Override
	public void removeService(Service service) {
		order.getServices().remove(service);
		order.setState(order.getOpen());
	}

	@Override
	public void printInvoiced() {
		order.setState(order.getInvoiced());
	}
}
