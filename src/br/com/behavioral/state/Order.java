package br.com.behavioral.state;

import java.util.ArrayList;
import java.util.List;

public class Order {
	// ARMAZENA O ULTIMO ID
	private static Integer ID_GENERATION = 1;
	private State open;
	private State closed;
	private State canceled;
	private State invoiced;
	private Integer id;
	private State state;
	private List<Service> services;

	public Order() {
		// EVENTO 1
		id = ID_GENERATION++;
		open = new Open(this);
		closed = new Closed(this);
		canceled = new Canceled(this);
		invoiced = new Invoiced(this);
		state = open;
		services = new ArrayList<Service>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<Service> getServices() {
		return services;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	// EVENT 2
	public void insertService(Service service) {
		// switch (state) {
		// case OPEN:
		// services.add(service);
		// break;
		// case CLOSED:
		// state = State.OPEN;
		// services.add(service);
		// break;
		// case CANCELED:
		// System.out.println("Impossible");
		// break;
		// case INVOICED:
		// System.out.println("Impossible");
		// break;
		// }
		state.insertService(service);
	}

	// EVENT 3
	public void removeService(Service service) {
		state.removeService(service);
	}

	// EVENT 3
	public void cancelOS() {
		state.cancelIOS();
	}

	// EVENT 4
	public void requestPayment() {
		state.requestPayment();
	}

	// EVENT 5
	public void printInvoice() {
		state.printInvoiced();
	}

	@Override
	public String toString() {
		return "**************** OS id: " + id + "************\n State :"
				+ state + "\n Services" + services + "\n\n";
	}

	public State getOpen() {
		return open;
	}

	public void setOpen(State open) {
		this.open = open;
	}

	public State getClosed() {
		return closed;
	}

	public void setClosed(State closed) {
		this.closed = closed;
	}

	public State getCanceled() {
		return canceled;
	}

	public void setCanceled(State canceled) {
		this.canceled = canceled;
	}

	public State getInvoiced() {
		return invoiced;
	}

	public void setInvoiced(State invoiced) {
		this.invoiced = invoiced;
	}

}
