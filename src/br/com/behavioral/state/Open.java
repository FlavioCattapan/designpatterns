package br.com.behavioral.state;

public class Open extends State {
	public Open(Order os) {
		super(os);
	}

	@Override
	public void insertService(Service service) {
		order.getServices().add(service);
	}

	@Override
	public void cancelIOS() {
		super.cancelIOS();
	}

	@Override
	public void requestPayment() {
		super.requestPayment();
	}

	@Override
	public void removeService(Service service) {
		super.removeService(service);
	}
}
