package br.com.behavioral.chainOfResponsibility;

public interface Desconto {
	
	double desconto(Orcamento orcamento);

	void setProximo(Desconto desconto);
}
