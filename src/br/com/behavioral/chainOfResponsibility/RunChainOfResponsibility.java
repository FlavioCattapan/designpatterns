package br.com.behavioral.chainOfResponsibility;

public class RunChainOfResponsibility {
	
//	O padr�o de projeto de software Chain of Responsibility representa um encadeamento 
//	de objetos receptores para o processamento de uma s�rie de solicita��es diferentes.
//	Esses objetos receptores passam a solicita��o ao longo da cadeia at� que um ou v�rios
//	objetos a tratem.

//	Cada objeto receptor possui uma l�gica descrevendo os tipos de solicita��o que � 
//	capaz de processar e como passar adiante aquelas que requeiram processamento por outros receptores.
//	A delega��o das solicita��es pode formar uma �rvore de recurs�o, com um mecanismo especial 
//	para inser��o de novos receptores no final da cadeia existente.

//	Dessa forma, fornece um acoplamento mais fraco por evitar a associa��o expl�cita do 
//	remetente de uma solicita��o ao seu receptor e dar a mais de um objeto a oportunidade 
//	de tratar a solicita��o.

//	Um exemplo da aplica��o desse padr�o � o mecanismo de heran�a nas linguagens orientadas 
//	a objeto: um m�todo chamado em um objeto � buscado na classe que implementa o objeto e,
//	se n�o encontrado, na superclasse dessa classe, de maneira recursiva.
//	Situa��es de utiliza��o

//	mais de um objeto pode tratar uma solicita��o e o objeto que a tratar� n�o � conhecido a priori;
//	o objeto que trata a solicita��o deve ser escolhido automaticamente;
//	deve-se emitir uma solicita��o para um dentre v�rios objetos, sem especificar explicitamente o receptor;
//	o conjunto de objetos que pode tratar uma solicita��o deveria ser especificado dinamicamente.

//	Em um sistema orientado a objetos esses interagem entre si atrav�s de mensagens, 
//	e o sistema necessita de determinar qual o objeto que ir� tratar a requisi��o. 
//	O padr�o de projeto Chain of Responsibility permite determinar quem ser� o objeto que ir� 
//	tratar a requisi��o durante a execu��o. Cada objeto pode tratar ou passar a mensagem para o
//	pr�ximo na cascata.

//	Em um escrit�rio, por exemplo, onde se tem 4 linhas telef�nicas, a primeira linha � o primeiro
//	objeto, a segunda linha � o segundo, e assim sucessivamente at� a grava��o autom�tica que � 
//	o quinto objeto. Se a primeira linha estiver dispon�vel ela ir� tratar a liga��o, se n�o 
//	ela passa a tarefa para o pr�ximo objeto, que � a segunda linha. Se essa estiver ocupada 
//	ela passa a tarefa para a pr�xima e assim sucessivamente at� que um objeto possa tratar a tarefa.

//	Nesse caso, se todas as linhas estiverem ocupadas o �ltimo objeto, que � a grava��o autom�tica,
//	tratar� da tarefa.
	
	public static void main(String[] args) {
		
		CalculadorDeDesconto descontos = new CalculadorDeDesconto();
		
		Orcamento orcamento = new Orcamento(600.00);
		orcamento.adicionaItem(new Item("CANETA", 250.00));
		orcamento.adicionaItem(new Item("LAPIS", 250.0));
		
		double descontoFinal = descontos.calcula(orcamento);
		
		System.out.println(descontoFinal);
		
	}
	

}
