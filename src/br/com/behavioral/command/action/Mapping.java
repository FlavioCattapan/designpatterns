package br.com.behavioral.command.action;

import java.util.HashMap;
import java.util.Map;

public class Mapping {
	
	private Map<String, Command> commands = new HashMap<String, Command>();

	public void register(String name, Command command) {
		commands.put(name, command);
	}

	public void execute(String name) {
		Command command = (Command) commands.get(name);
		if (command != null) {
			command.execute();
		}
	}
}
