package br.com.behavioral.command.action;

public interface Command {
	// Todo command deve ter um execute
	public void execute();
}
