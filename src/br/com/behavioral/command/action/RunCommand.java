package br.com.behavioral.command.action;

public class RunCommand {
	public static void main(String[] args) {
		
		Mapping mapping = new Mapping();
		mapping.register("/create", new CreateAction());
		mapping.register("/remove", new RemoveAction());
		mapping.execute("/create");
		mapping.execute("/remove");
		
		
//		Objetivo
//		Encapsular uma solicita��o como um objeto, 
//		desta forma permitindo que clientes parametrizem diferentes solicita��es, 
//		enfileirem ou fa�am o registro (log) de solicita��es e suportem opera��es que podem ser desfeitas.

//		Problema
//		Algumas vezes � necess�rio emitir solicita��es para objetos
//		nada sabendo sobre a opera��o que est� sendo solicitada ou sobre o receptor da mesma.

//		Utilizar quando
//		Parametrizar objetos por uma a��o a ser executada. Voc� pode expressar tal parametriza��o
//		numa linguagem procedural atrav�s de uma fun��o callback, ou seja, uma fun��o que �
//		registrada em algum lugar para ser chamada em um momento mais adiante.
//		Os Commands s�o uma substitui��o orientada a objetos para callbacks;
//		Especificar, enfileirar e executar solicita��es em tempos diferentes.
//		Um objeto Command pode ter um tempo de vida independente da solicita��o original.
//		Se o receptor de uma solicita��o pode ser representado de uma maneira independente do 
//		espa�o de endere�amento, ent�o voc� pode transferir um objeto Command para a solicita��o
//		para um processo diferente e l� atender a solicita��o;
//		Suportar desfazer opera��es. A opera��o Execute, de Command, pode armazenar estados 
//		para reverter seus efeitos no pr�prio comando. A interface do Command deve ter
//		acrescentada uma opera��o Unexecute, que o reverte.efeitos de uma chamada anterior de Execute.
//		Os comandos executados s�o armazenados em uma lista hist�rica. O n�vel ilimitado de 
//		desfazer e refazer opera��es � obtido percorrendo esta lista para tr�s e para frente, 
//		chamando opera��es Unexecute e Execute, respectivamente.
		
//		Aplica��o
//		A chave deste padr�o � uma classe abstrata Command, a qual declara uma interface
//		para execu��o de opera��es. Na sua forma mais simples, esta interface inclui uma 
//		opera��o abstrata Execute. As subclasses concretas de Command especificam um par 
//		receptor-a��o atrav�s do armazenamento do receptor como uma vari�vel de inst�ncia
//		e pela implementa��o de Execute para invocar a solicita��o. O receptor tem o conhecimento 
//		necess�rio para poder executar a solicita��o.

//		Sin�nimos
//		Tamb�m conhecido como Action, Transaction (A��o, Transa��o, respectivamente).
		
		
	}
}
