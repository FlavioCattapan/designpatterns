package br.com.behavioral.command.pedido;

public enum Status {
	
	NOVO, PROCESSANDO, PAGO, ITEM_SEPARADO, ENTREGE;

}
