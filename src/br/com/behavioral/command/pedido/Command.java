package br.com.behavioral.command.pedido;

public interface Command {

	void executa();
	
}
