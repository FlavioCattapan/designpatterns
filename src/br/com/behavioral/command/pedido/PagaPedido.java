package br.com.behavioral.command.pedido;

public class PagaPedido implements Command{
	
	private Pedido pedido;

	public PagaPedido(Pedido pedido) {
		super();
		this.pedido = pedido;
	}

	@Override
	public void executa() {
		System.out.println("Pagando pedido do :"+pedido.getCliente());
		pedido.paga();
	}
	
}
