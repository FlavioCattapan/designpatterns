package br.com.behavioral.command.pedido;

import java.util.ArrayList;
import java.util.List;

public class FilaDeTrabalho {
	
	private List<Command> listaComandos;
	
	public FilaDeTrabalho() {
		listaComandos = new ArrayList<Command>();
	}
	
	public void adiciona(Command command){
		this.listaComandos.add(command);
	}
	
	public void processa(){
		for(Command command : listaComandos){
			command.executa();
		}
	}
	
	

}
