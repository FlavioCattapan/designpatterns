package br.com.behavioral.command.pedido;

public class ConcluiPedido implements Command{
	
	private Pedido pedido;

	public ConcluiPedido(Pedido pedido) {
		super();
		this.pedido = pedido;
	}

	@Override
	public void executa() {
		System.out.println("Concluindo pedido do :"+pedido.getCliente());
		pedido.finaliza();
	}
	
}
