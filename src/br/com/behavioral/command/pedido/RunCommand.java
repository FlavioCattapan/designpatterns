package br.com.behavioral.command.pedido;

public class RunCommand {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Command encapsula comando a ser executado no futuro

		Pedido pedido = new Pedido("Mauricio", 150.00);
		Pedido pedido2 = new Pedido("Marcelo", 250.00);
		
		FilaDeTrabalho filaDeTrabalho = new FilaDeTrabalho();
		
		filaDeTrabalho.adiciona(new PagaPedido(pedido));
		filaDeTrabalho.adiciona(new PagaPedido(pedido2));
		filaDeTrabalho.adiciona(new ConcluiPedido(pedido));
		
		filaDeTrabalho.processa();
		
		
	}

}
