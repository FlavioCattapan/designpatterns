package br.com.behavioral.command.xls;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

public class OpenCommand extends AbstractAction{

	private XlsDocumment xlsDocumment;
	
	public OpenCommand(XlsDocumment xlsDocumment) {
		super("open");
		this.xlsDocumment = xlsDocumment;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		String file = JOptionPane.showInputDialog(arg0.getSource(), "Entre com o nome do arquivo !");
		
		if(file != null && file.length() > 0){
			
			xlsDocumment.open(file);
			
		}
		
	}

}
