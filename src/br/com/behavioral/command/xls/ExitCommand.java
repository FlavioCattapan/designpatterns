package br.com.behavioral.command.xls;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

public class ExitCommand extends AbstractAction{

	public ExitCommand() {
		super("exit");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("EXIT");
	}

	
	
	
}
