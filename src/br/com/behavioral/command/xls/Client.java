package br.com.behavioral.command.xls;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

public class Client extends JFrame {
	
	public Client(JMenuBar menubar) {
		setTitle("Sample Command");
		setSize(500,250);
		setJMenuBar(menubar);
		setVisible(true);
	}
	
	
	public static void main(String[] args) {
		
		JMenuBar jMenuBar = new JMenuBar();
		
		JMenu jMenu = new JMenu("Command");
		
		XlsDocumment xlsDocumment = new XlsDocumment();
		
		OpenCommand openCommand = new OpenCommand(xlsDocumment);
		
		ExitCommand exitCommand = new ExitCommand();
		
		jMenu.add(exitCommand);
		jMenu.add(openCommand);
		
		jMenuBar.add(jMenu);
		
		new Client(jMenuBar);
		
		
	}
	
	

}
