package br.com.behavioral.observer;

// Classe observada //SOURCE // SUBJECT
public class Telephone {
	private TelephoneListener listener;

	public void setTelephoneListener(TelephoneListener listener) {
		this.listener = listener;
	}

	public void isTouched() {
		// code
		listener.telephoneTouched();
	}
}
