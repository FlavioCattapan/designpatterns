package br.com.behavioral.observer;

//Observer como  a pessoa quer observar o telefone ela deve impl 
public class Pessoa implements TelephoneListener {

	public void observeTelephone(Telephone telephone) {
		telephone.setTelephoneListener(this);
	}

	@Override
	public void telephoneTouched() {
		System.out.println("EU ATENDO......");
	}
}
