package br.com.behavioral.observer;

public class TelefhoneExchange {
	public static void main(String[] args) {

		// O Observer � um padr�o de projeto de software que define uma
		// depend�ncia
		// um-para-muitos entre objetos de modo que quando um objeto muda o
		// estado,
		// todos seus dependentes s�o notificados e atualizados automaticamente.
		// Permite que objetos interessados sejam avisados da mudan�a de estado
		// ou
		// outros eventos ocorrendo num outro objeto.

		// O padr�o Observer � tamb�m chamado de Publisher-Subscriber,
		// Event Generator e Dependents.

		Telephone telephone = new Telephone();
	
		Pessoa pessoa = new Pessoa();
		pessoa.observeTelephone(telephone);
		
		Pessoa pessoa2 = new Pessoa();
		pessoa2.observeTelephone(telephone);
		
		telephone.isTouched();

		// Um objeto que possua agrega��es deve permitir que seus elementos
		// sejam acessados sem que
		// a sua estrutura interna seja exposta. De uma maneira geral pode-se
		// desejar que estes elementos
		// sejam percorridos em v�rias ordens.
		// Como garantir que objetos que dependem de outro objeto percebam as
		// mudan�as naquele objeto?

		// Os observadores (observer) devem conhecer o objeto de interesse.
		// O objeto de interesse (subject) deve notificar os observadores quando
		// for atualizado.

		// Os objetos devem interligar-se entre si de forma a que n�o se
		// conhe�am em tempo de
		// compila��o de forma a criar o acoplamento e desfaz�-lo a qualquer
		// momento em tempo de execu��o.
		// Solucionar isso fornece uma implementa��o muito flex�vel de
		// acoplamento de abstra��es.
		
		
		// O padr�o Observer pode ser usado quando uma abstra��o tem dois
		// aspectos,
		// um dependente do outro. Encapsular tais aspectos em objetos separados
		// permite
		// que variem e sejam reusados separadamente. Quando uma mudan�a a um
		// objeto requer
		// mudan�as a outros e voc� n�o sabe quantos outros objetos devem mudar
		// ou quando um
		// objeto deve ser capaz de avisar outros sem fazer suposi��es sobre
		// quem s�o os objetos.
		// Em outras palavras, sem criar um acoplamento forte entre os objetos.

	}
}
