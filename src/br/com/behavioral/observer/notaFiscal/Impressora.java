package br.com.behavioral.observer.notaFiscal;

public class Impressora implements AcoesAposGerarNota {

	@Override
	public void executa(NotaFiscal notaFiscal) {
		System.out.println("Nota impressa");
	}

}
