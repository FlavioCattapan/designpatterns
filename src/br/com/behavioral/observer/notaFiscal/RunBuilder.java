package br.com.behavioral.observer.notaFiscal;

public class RunBuilder {

	public static void main(String[] args) {

		// A��es que devem ser executadas ap�s um comportamento
		// As a��es s�o os observadores
		
		NotaFiscalBuilder notalFiscalBuilder = new NotaFiscalBuilder();
		notalFiscalBuilder.adicionaAcao(new EnviadorDeEmail());
		notalFiscalBuilder.adicionaAcao(new EnviadorDeSms());
		notalFiscalBuilder.adicionaAcao(new Impressora());
		
		notalFiscalBuilder.paraEmpresa("RichNet")
		.comCnpj("11.125.685/0001-12")
		.comItem(new ItemDaNota("Item 1", 200.00))
		.comItem(new ItemDaNota("Item 2", 300.00))
		.comObservacoes("obs")
		.naDataAtual();
		
		NotaFiscal notaFiscal = notalFiscalBuilder.constroi();
		
		System.out.println(notaFiscal.getValorBruto());
		
		
		

	}

}
