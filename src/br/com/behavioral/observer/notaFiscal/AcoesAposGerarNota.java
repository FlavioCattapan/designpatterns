package br.com.behavioral.observer.notaFiscal;

public interface AcoesAposGerarNota {
	
	void executa(NotaFiscal notaFiscal);

}
