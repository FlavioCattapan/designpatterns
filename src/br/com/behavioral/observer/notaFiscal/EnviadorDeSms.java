package br.com.behavioral.observer.notaFiscal;

public class EnviadorDeSms implements AcoesAposGerarNota {

	@Override
	public void executa(NotaFiscal notaFiscal) {
		System.out.println("enviei por sms");
	}

}
