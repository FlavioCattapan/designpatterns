package br.com.behavioral.observer.notaFiscal;

public class EnviadorDeEmail implements AcoesAposGerarNota {

	@Override
	public void executa(NotaFiscal notaFiscal) {
		System.out.println("Enviei por email");
	}

}
