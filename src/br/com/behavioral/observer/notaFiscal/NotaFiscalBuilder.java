package br.com.behavioral.observer.notaFiscal;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NotaFiscalBuilder {

	private String razaoSocial;
	private String cnpj;
	private List<ItemDaNota> todosItens = new ArrayList<ItemDaNota>();
    private double valorBruto;
    private double impostos;
    private String observacoes;
    private Calendar data;
    private List<AcoesAposGerarNota> listaAcoesAposGerarNotas;
    
    public NotaFiscalBuilder() {
    	this.listaAcoesAposGerarNotas = new ArrayList<AcoesAposGerarNota>();
	}
    
    public NotaFiscalBuilder paraEmpresa(String razaoSocial){
    	this.razaoSocial = razaoSocial;
    	return this;
    }
    
    public void adicionaAcao(AcoesAposGerarNota acoesAposGerarNota){
    	this.listaAcoesAposGerarNotas.add(acoesAposGerarNota);
    }
    
    public NotaFiscalBuilder comCnpj(String cnpj){
    	this.cnpj = cnpj;
    	return this;
    }
    
    public NotaFiscalBuilder comItem(ItemDaNota itensDaNota){
    	todosItens.add(itensDaNota);
    	valorBruto += itensDaNota.getValor();
    	impostos += itensDaNota.getValor() * 0.05;
    	return this;
    }
	
    public NotaFiscalBuilder comObservacoes(String observacoes){
    	this.observacoes = observacoes;
    	return this;
    }
    
    public NotaFiscalBuilder naDataAtual(){
    	this.data = Calendar.getInstance();
    	return this;
    }
    
    public NotaFiscal constroi(){
    	NotaFiscal notaFiscal = new NotaFiscal(razaoSocial, cnpj, data, valorBruto, impostos, todosItens, observacoes);
    	
    	for(AcoesAposGerarNota acoesAposGerarNota : listaAcoesAposGerarNotas){
    		acoesAposGerarNota.executa(notaFiscal);
    	}
    	
    	
    	return 	notaFiscal;
    }
	
}
