package br.com.behavioral.observer;

// Classe Observadora
public interface TelephoneListener {
	public void telephoneTouched();
}
