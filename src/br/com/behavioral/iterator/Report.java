package br.com.behavioral.iterator;

public class Report {
	public static void main(String[] args) {
		/*
		 * BasketFruits basketFruits = new BasketFruits(); ArrayList<Product>
		 * fruits = basketFruits.getProducts();
		 * 
		 * BasketDrinks basketDrinks = new BasketDrinks(); Product[] drinks =
		 * basketDrinks.getDrinks();
		 * 
		 * for (int i = 0; i < fruits.size(); i++) { Product fruit = (Product)
		 * fruits.get(i); System.out.println(fruit.getName() + " , ");
		 * System.out.println(fruit.getDescription() + " , ");
		 * System.out.println(fruit.getPrice()); }
		 * 
		 * for (int i = 0; i < drinks.length; i++) { Product drink = drinks[i];
		 * 
		 * if (drink != null) { System.out.println(drink.getName());
		 * System.out.println(drink.getDescription());
		 * System.out.println(drink.getPrice()); }
		 * 
		 * }
		 */
		Report report = new Report();
		report.printProducts(new BasketFruits().getProducts());
		report.printProducts(new BasketDrinks().getDrinks());

		// Fornecer um meio de acessar, sequencialmente, os elementos de um
		// objeto
		// agregado sem expor sua representa��o subjacente

		// Ent�o utilizando o padr�o Iterator n�s poderemos acessar os
		// elementos de um conjunto de dados sem conhecer sua implementa��o,
		// ou seja, sem a necessidade de saber se ser� utilizado ArrayList ou
		// Matriz.
		// No nosso exemplo os objetos agregados seriam as listas de canais
		// (ArrayList e Matriz).
	}

	public void printProducts(Iterator iterator) {
		while (iterator.hasNex()) {
			Product product = (Product) iterator.next();
			System.out.println(product.getName());
			System.out.println(product.getDescription());
			System.out.println(product.getPrice());
		}
	}
}
