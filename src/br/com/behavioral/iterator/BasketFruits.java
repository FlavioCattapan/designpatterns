package br.com.behavioral.iterator;

import java.util.ArrayList;
import javax.swing.text.StyledEditorKit.ItalicAction;

public class BasketFruits {
	public BasketFruits() {
		fruits = new ArrayList<Product>();
		addProduct("Aple", "very good", 10);
		addProduct("Orange", "bla bla bla", 12.50);
		addProduct("Lemon", "Very good too ", 12.40);
	}

	private ArrayList<Product> fruits;

	public void addProduct(String name, String description, double price) {
		Product product = new Product(name, description, price);
		fruits.add(product);
	}

	public Iterator getProducts() {
		return new FruitsIterator(fruits);
	}
}
