package br.com.behavioral.iterator;

public class BasketDrinks {
	static final int MAX_PRODUCTS = 4;
	int index = 0;
	Product[] drinks;

	public BasketDrinks() {
		drinks = new Product[MAX_PRODUCTS];
		addProduct("Wi blane", "bla bla bla", 25.90);
		addProduct("Beer", "bla bla bla", 2.40);
	}

	public void addProduct(String name, String description, double price) {
		Product product = new Product(name, description, price);
		if (index < MAX_PRODUCTS) {
			drinks[index] = product;
			index++;
		}
	}

	public Iterator getDrinks() {
		return new DrinksIterator(drinks);
	}
}
