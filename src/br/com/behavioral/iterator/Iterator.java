package br.com.behavioral.iterator;

public interface Iterator {
	boolean hasNex();

	Object next();
}
