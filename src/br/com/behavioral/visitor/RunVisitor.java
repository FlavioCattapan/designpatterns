package br.com.behavioral.visitor;

public class RunVisitor {
	
	public static void main(String[] args) {
		
//		Visita uma �rvore para fazer algo.
		
//		Representa uma opera��o a ser realizada sobre elementos da estrutura de um objeto. 
//		O Visitor permite que se crie uma nova opera��o sem que se mude a classe dos elementos 
//		sobre as quais ela opera. � uma maneira de separar um algoritmo da estrutura de um objeto.
//		Um resultado pr�tico � a habilidade de adicionar novas funcionalidades a estruturas de um 
//		objeto pr�-existente sem a necessidade de modific�-las.

//		A id�ia � usar uma classe de elementos como uma estrutura, 
//		sendo que cada uma delas possui um m�todo cujo um dos argumentos � um objeto do tipo visitor.
//		Visitor � uma interface que possui um m�todo visit() para cada classe de elementos.
//		O m�todo accept() de uma classe de elementos invoca o m�todo visit() de sua respectiva classe. 
//		Classes visitor concretas distintas podem ent�o ser escritas para implementar opera��es 
//		especiais.

//		O padr�o Visitor � uma solu��o para separar o algoritmo da estrutura.
//		Uma das vantagens desse padr�o � a habilidade de adicionar novas opera��es a uma
//		estrutura j� existente. Com ele, podemos ter a classe ObjetoSolido e o comportamento
//		de queda em uma classe Gravidade, separada da estrutura do ObjetoSolido.
//		Isso � feito atrav�s de uma interface, onde o objeto que vai executar esse m�todo da 
//		classe do comportamento, passa uma referencia dela mesmo junto dos par�metros normais da classe. 
	
		ArvoreBinaria arvore = new ArvoreBinaria(7);

		arvore.inserir(15);
		arvore.inserir(10);
		arvore.inserir(5);
		arvore.inserir(2);
		arvore.inserir(1);
		arvore.inserir(20);

		System.out.println("### Exibindo em ordem ###");
		arvore.aceitarVisitante(new ExibirInOrderVisitor());
		System.out.println("### Exibindo pre ordem ###");
		arvore.aceitarVisitante(new ExibirPreOrdemVisitor());
		System.out.println("### Exibindo p�s ordem ###");
		arvore.aceitarVisitante(new ExibirPostOrderVisitor());
		System.out.println("### Exibindo identado ###");
		arvore.aceitarVisitante(new ExibirIndentadoVisitor());
	}

}
